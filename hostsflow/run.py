#!/usr/bin/python3
from argparse import ArgumentParser
from actions import connect,clean
from sys import argv


def run():
    parser = ArgumentParser(epilog="for cleaning:\n {} clean".format(argv[0]))

    parser.add_argument("interface", type=str, help="external interface of host machine (example : wlan0)")
    parser.add_argument("-c", "--collector", type=str, help="sflow collector's ip", default="0.0.0.0")
    parser.add_argument("-x", "--cpu", type=str, help="cpu docker", default="0.1")
    parser.add_argument("-m", "--memory", type=str, help="memory docker", default="0.1")
    parser.add_argument("-n", "--sampling_rate", type=int, help="n where 1/n packets will be sent to collector",
                        default=3)
    parser.add_argument("--polling", type=int, help="Polling",default=10)

    args = parser.parse_args()

    start_successful = \
        connect(args.interface, args.memory, args.cpu , args.polling,args.sampling_rate,
                 args.collector)

    if start_successful:
        print("Successfully started.\n")
    else:
        print("Failed to start.\n")
        success = clean()
        print("Clean {}".format("successful" if success else "failed."))


def print_help():
    print("{} clean\n or \n".format(argv[0]))
    run()


if __name__ == '__main__':
    if len(argv) > 1 and argv[1] == "clean":
        clean()
    else:
        run()
