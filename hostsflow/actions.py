#
from subprocess import call
from os import environ


def connect(public_interface: str, memory: str, cpu: str, polling: int,sampling_rate: int,
            collector: str) -> bool:

    environ["PUBLIC_INTERFACE"] = public_interface
    environ["CONTAINER_MEMORY"] = memory
    environ["CONTAINER_CPUS"] = cpu
    environ["POLLING"] = str(polling)
    environ["SAMPLING"] = str(sampling_rate)
    environ["COLLECTOR_IP"] = collector
    environ["COLLECTOR_PORT"] = "6343"
    environ["CONTAINER"] = "bkimminich/juice-shop:latest"
    return call("bash/connect.sh") == 0


def clean() -> bool:
    return call("bash/cleanup.sh") == 0
