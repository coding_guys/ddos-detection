#!/bin/bash
function setup_docker {
    # Add repository for installing the docker
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

    apt-get update && apt-get upgrade -y

    # Make sure you are about to install from the Docker repo
    # instead of the default Ubuntu 16.04 repo:
    apt-cache policy docker-ce

    echo "=== INSTALLING DOCKER ==="
    apt-get install -y docker-ce
}

function setup_hostsflow {
    echo "=== INSTALLING HOSTSFLOW==="

    sudo apt install \
        libnfnetlink-dev \
        libpcap-dev \
        dbus-dev \
        openssl-dev \
        dbus-dev \
        libsystemd-daemon-dev -y

    git clone https://github.com/sflow/host-sflow
    cd host-sflow
    make
    sudo make install
    cd ..
}

function setup_tools {
echo "=== INSTALLING USEFULL TOOLS ==="
apt-get install -y \
  iputils-ping \
  net-tools \
  traceroute \
  tcpdump \
  git \
  tree \
  hping3 \
  python3-pip \
  tmux
}

function setup_repo {
    echo "=== CLONING PROJECT REPO ==="
    git clone git@bitbucket.org:coding_guys/ddos-detection.git

    echo "=== PULLING DOCKER IMAGES ==="
    docker pull bkimminich/juice-shop:latest
}

function setup_all {
    setup_docker
    setup_hostsflow
    setup_tools
    setup_repo
}
