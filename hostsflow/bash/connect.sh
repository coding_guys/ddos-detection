#!/bin/bash
source bash/utils.sh
set -e

check_if_envs_exist PUBLIC_INTERFACE \
                    CONTAINER_MEMORY \
                    CONTAINER_CPUS \
                    POLLING \
                    SAMPLING \
                    COLLECTOR_IP \
                    COLLECTOR_PORT \
                    CONTAINER 

ULOG_GROUP="1"
ULOG_PROBAB="0.0025"

MOD_STATISTIC="-m statistic --mode random --probability $ULOG_PROBAB"
ULOG_CONFIG="--nflog-group $ULOG_GROUP --nflog-prefix SFLOW --nflog-threshold 1"




echo Starting container
sudo docker run -d -it \
       --name=container1 \
       --memory=${CONTAINER_MEMORY} \
       --cpus=${CONTAINER_CPUS} \
       -p 0.0.0.0:80:3000 \
       --cap-add NET_ADMIN \
        ${CONTAINER}


echo "Saving hostsflow config"
cat bash/hsflowd.conf  | \
    sed s/IFACE/${PUBLIC_INTERFACE}/g |\
    sed s/POLLING/${POLLING}/g |\
    sed s/SAMPLING/${SAMPLING}/g |\
    sed s/COLLECTOR_PORT/${COLLECTOR_PORT}/g |\
    sed s/COLLECTOR_IP/${COLLECTOR_IP}/g |\
    sed s/ULOG_GROUP/${ULOG_GROUP}/g  |\
    sed s/ULOG_PROBAB/${ULOG_PROBAB}/g | sudo tee /etc/hsflowd.conf

echo "Starting hostsflow service"
sudo systemctl start hsflowd

echo "Adding iptables NFLOG entries"
sudo iptables -I INPUT -j NFLOG $MOD_STATISTIC $ULOG_CONFIG
sudo iptables -I OUTPUT -j NFLOG $MOD_STATISTIC $ULOG_CONFIG
