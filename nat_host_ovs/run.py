#!/usr/bin/python3
from argparse import ArgumentParser
from actions import *
from sys import argv


def run():
    parser = ArgumentParser(epilog="for cleaning:\n {} clean".format(argv[0]))

    parser.add_argument("new_ip_address", type=str,
                        help="desired address of container1 in network (example : 192.168.0.5/24)")
    parser.add_argument("interface", type=str, help="external interface of host machine (example : wlan0)")
    parser.add_argument("-c", "--collector", type=str, help="sflow collector's ip", default="0.0.0.0")
    parser.add_argument("-x", "--cpu", type=str, help="cpu docker", default="0.1")
    parser.add_argument("-m", "--memory", type=str, help="memory docker", default="0.1")
    parser.add_argument("-p", "--pat", help="Should use pat instead of static nat?", action="store_true")
    parser.add_argument("-n", "--sampling_rate", type=int, help="n where 1/n packets will be sent to collector",
                        default=3)

    args = parser.parse_args()

    start_successful = \
        start_daemons() and \
        enable_ip_forwarding() and \
        setup_ovs_and_containers(args.new_ip_address, args.interface,args.pat,args.memory,args.cpu) and \
        configure_sflow(args.collector, args.sampling_rate)

    if start_successful:
        print("Successfully started.\n")
    else:
        print("Failed to start.\n")
        success = clean()
        print("Clean {}".format("successful" if success else "failed."))


def print_help():
    print("{} clean\n or \n".format(argv[0]))
    run()


if __name__ == '__main__':
    if len(argv) > 1 and argv[1] == "clean":
        clean()
    else:
        run()
