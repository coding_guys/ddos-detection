#!/bin/bash
source bash/utils.sh
set -e

check_if_envs_exist NEW_PUBLIC_IP NEW_PUBLIC_IP_MASK PUBLIC_INTERFACE CONTAINER_MEMORY CONTAINER_CPUS USE_PAT

# NEW_PUBLIC_IP - ip pod jakim jest widoczny kontener w sieci zewnętrznej ( inside global )


# https://developer.ibm.com/recipes/tutorials/using-ovs-bridge-for-docker-networking/

export privateintf=ovs-br1
IP_1="192.168.5.2" # IP in docker subnet

sudo ovs-vsctl add-br ovs-br1

sudo ifconfig ovs-br1 192.168.5.1 netmask 255.255.255.0 up

echo Starting containers
docker run -d -it --name=container1 --net=none --memory=${CONTAINER_MEMORY} --cpus=${CONTAINER_CPUS} --cap-add NET_ADMIN bkimminich/juice-shop:latest

echo Connecting containers to OvS 
# eth0 - port inside container
sudo ovs-docker add-port ovs-br1 eth0 container1 --ipaddress=${IP_1}/24 --gateway=192.168.5.1

################
# NATing:
###############

echo "Nating public $PUBLIC_INTERFACE with private $privateintf"

#Dynamic nat for everything
sudo -E iptables -t nat -A POSTROUTING -o ${PUBLIC_INTERFACE} -j MASQUERADE
sudo -E iptables -A FORWARD -i ${privateintf} -j ACCEPT
sudo -E iptables -A FORWARD -i ${privateintf} -o ${PUBLIC_INTERFACE} -m state --state RELATED,ESTABLISHED -j ACCEPT


#STATIC NAT 1:1
function staticNat {
    #http://www.cahilig.net/2010/10/28/how-enable-11-nat-iptables
    #https://www.ostechnix.com/how-to-assign-multiple-ip-addresses-to-single-network-card-in-linux/

    echo "Using SNAT"
    iptables -t nat -A POSTROUTING -o ${PUBLIC_INTERFACE} -s ${IP_1} -j SNAT --to-source ${NEW_PUBLIC_IP}
    iptables -t nat -A PREROUTING -i ${PUBLIC_INTERFACE} -d ${NEW_PUBLIC_IP} -j DNAT --to-destination ${IP_1}
    iptables -A FORWARD -s ${NEW_PUBLIC_IP} -j ACCEPT
    iptables -A FORWARD -d ${IP_1} -j ACCEPT
    sudo ip addr add ${NEW_PUBLIC_IP}/${NEW_PUBLIC_IP_MASK} dev ${PUBLIC_INTERFACE}
}

#STATIC PAT
function pat {
    echo "Using PAT"
    iptables -t nat -A PREROUTING -p tcp -d ${NEW_PUBLIC_IP} --dport 9999 -j DNAT --to-destination ${IP_1}:3000
}

if [ $USE_PAT -eq 1 ]
then
    pat
else
    staticNat
fi

