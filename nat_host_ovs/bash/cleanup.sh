#!/bin/bash

docker stop container1
docker rm container1

#sudo ./ip_forwarding.sh -d

#  Overkill - removes entries that make docker connection to internet work.
#  temporary solution : restart docker service
sudo iptables -F
sudo iptables -X
sudo iptables -t nat -F
sudo iptables -t nat -X
sudo iptables -t mangle -F
sudo iptables -t mangle -X
sudo iptables -P INPUT ACCEPT
sudo iptables -P FORWARD ACCEPT
sudo iptables -P OUTPUT ACCEPT

sudo ovs-vsctl del-br ovs-br1

sudo service docker restart
