#!/bin/bash
set -e
source bash/utils.sh

check_if_envs_exist COLLECTOR_IP COLLECTOR_PORT AGENT_IP HEADER_BYTES SAMPLING_N POLLING_SECS

sudo ovs-vsctl -- --id=@sflow create sflow agent=${AGENT_IP} \
    target="\"${COLLECTOR_IP}:${COLLECTOR_PORT}\"" header=${HEADER_BYTES} \
    sampling=${SAMPLING_N} polling=${POLLING_SECS} \
      -- set bridge ovs-br1 sflow=@sflow
