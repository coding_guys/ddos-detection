#!/bin/bash

if [[ $# -eq 1 && ( "$1" == "-d" || "$1" == "--disable") ]]
then
  cat /etc/sysctl.conf | sed "s/\$net.ipv4.ip_forward=1/\$#net.ipv4.ip_forward=1/" | sudo tee /etc/sysctl.conf >& /dev/null
  sudo sh -c "echo 0 > /proc/sys/net/ipv4/ip_forward"
  echo "disabled ip forwarding"
else
  cat /etc/sysctl.conf | sed "s/#net.ipv4.ip_forward=1/net.ipv4.ip_forward=1/" | sudo tee /etc/sysctl.conf >& /dev/null
  sudo sh -c "echo 1 > /proc/sys/net/ipv4/ip_forward"
  echo "enabled ip forwarding"
fi
