#
from subprocess import call
from os import environ


def start_daemons() -> bool:
    return call("bash/daemons.sh") == 0


def enable_ip_forwarding() -> bool:
    return call("bash/ip_forwarding.sh") == 0


def setup_ovs_and_containers(new_public_ip_with_mask: str, public_interface: str,use_pat:bool,memory:str,cpu:str) -> bool:
    new_public_ip, new_mask = new_public_ip_with_mask.split("/")

    environ["NEW_PUBLIC_IP"] = new_public_ip
    environ["NEW_PUBLIC_IP_MASK"] = new_mask
    environ["PUBLIC_INTERFACE"] = public_interface
    environ["CONTAINER_MEMORY"] = "200m" if memory is None else memory
    environ["CONTAINER_CPUS"] = "0.1" if cpu is None else cpu
    environ["USE_PAT"]= "1" if use_pat else "0"
    return call("bash/connect.sh") == 0


def configure_sflow(collector_ip: str, sampling_rate: int) -> bool:
    environ["COLLECTOR_IP"] = collector_ip
    environ["COLLECTOR_PORT"] = "6343"
    environ["AGENT_IP"] = "lo"
    environ["HEADER_BYTES"] = "128"
    environ["SAMPLING_N"] = str(sampling_rate)
    environ["POLLING_SECS"] = "10"
    return call("bash/sflow_config.sh") == 0


def clean() -> bool:
    return call("bash/cleanup.sh") == 0

