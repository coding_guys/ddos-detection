#!/bin/bash

# Add repository for installing the docker
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

apt-get update && apt-get upgrade -y

# Make sure you are about to install from the Docker repo
# instead of the default Ubuntu 16.04 repo:
apt-cache policy docker-ce

echo "=== INSTALLING DOCKER ==="
apt-get install -y docker-ce

echo "=== INSTALLING OVS ==="
apt-get install -y openvswitch-switch

echo "=== INSTALLING USEFULL TOOLS ==="
apt-get install -y \
  iputils-ping \
  net-tools \
  traceroute \
  tcpdump \
  git \
  tree \
  hping3 \
  python3-pip \
  tmux

cd ~

echo "=== CLONING PROJECT REPO ==="
git clone git@bitbucket.org:coding_guys/ddos-detection.git

echo "=== CLONING DOTFILES REPO ==="
git clone https://Porcupine96@bitbucket.org/Porcupine96/dotfiles.git

echo "=== PULLING DOCKER IMAGES ==="
docker pull bkimminich/juice-shop:latest

